package prova_gradle_war;

import package1.Book;

import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("hello")
public class HelloWorldResource {

    // Eh sembla que es un d'aquests per cada client!!!!! No com a servlets! Uau. Ah no ser que amb scopes...

    @Context
    private ServletContext servletContext; // INJEEECT without weld


    // curl http://127.0.0.1:8080/proj1/restful-services/hello
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String helloWorld(){

        return "Hello World: "+ servletContext.getContextPath();
    }

    // curl http://127.0.0.1:8080/proj1/restful-services/hello/123?param1=Hola
    @GET
    @Path("/{id}")
    @Produces(MediaType.TEXT_PLAIN) // crec que només és per omplir un camp del header de la response...
    public String getUserById(@PathParam("id") Long id, @QueryParam("param1") String param1) {

        return id + "-" + param1;

    }

    // curl --data "EOOOOOO" http://127.0.0.1:8080/proj1/restful-services/hello
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public Response helloWorld2(String text){ // MIRA LO DEL POST!, SIMPLEMENT POSANT UN PARAMETRE...!

        // Es pot tornar una Response que torna una String/Integer/JAXBElement...
        // ó una String/Integer/JAXBElement directament

        Response response = Response.status(200).entity("Hello World " + text).build();
        return response;
    }

    // curl --data "Name=Mark&Surname=Brown" http://127.0.0.1:8080/proj1/restful-services/hello/howareyou
    @POST
    @Path("/howareyou")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response helloWorld3(MultivaluedMap<String,String> postData){

        Response response = Response.status(200).entity("How are you " + postData.get("Name").get(0) + "?").build();
        return response;
    }

    // curl --data "Name=Mark&Surname=Brown" http://127.0.0.1:8080/proj1/restful-services/hello/fine
    @POST
    @Path("/fine")
    @Produces(MediaType.APPLICATION_FORM_URLENCODED)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response helloWorld4(MultivaluedMap<String,String> postData){

        MultivaluedMap<String,String> form = new MultivaluedHashMap<>();
        form.add("Name","CorrectlyRead");
        form.add("Age", "CorrectlyRead");

        Response response = Response.status(200).entity(form).build();
        return response;
    }

    // Lo ultim que he fet ha estat jugar amb el MultivaluedMap<String,String> que és un del suportats per defecte,
    // MultivaluedMap<String,Object> no ho és!, te l'hauries de currar tu...
    // Hi ha una simetria entre... l'objecte que envio desde el client, i el objecte que rebo aqui...

    // Lo següent seria jugar amb objectes XML a rebre i retornar... amb JAXB...:

    // curl http://127.0.0.1:8080/proj1/restful-services/hello/jaxbelementexample
    @GET
    @Path("/jaxbelementexample")
    @Produces(MediaType.APPLICATION_XML)
    public Book getJaxbElementExample() {

        Book b1 = new Book();

        b1.setId(new Long(1234));
        b1.setName("The horror of the house");
        b1.setAuthor("Stephen King");

        return b1;

    }

    //curl -H "Content-Type: application/xml" -d '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><book id="5678"><author>Ken Follet</author><title>Never</title></book>' http://127.0.0.1:8080/proj1/restful-services/hello/addbook
    @POST
    @Path("/addbook")
    @Produces(MediaType.APPLICATION_XML)
    @Consumes(MediaType.APPLICATION_XML)
    public Book addBook (Book book){
        Book b1 = new Book();

        b1.setId(book.getId()+1);
        b1.setName(book.getName());
        b1.setAuthor(book.getAuthor());

        return b1;
    }
}
