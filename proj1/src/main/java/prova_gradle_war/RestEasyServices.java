package prova_gradle_war;

import javax.ws.rs.core.Application;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class RestEasyServices extends Application{

    //private Set<Object> singletons = new HashSet<>();
    //private Set<Class<?>> classes;

    public RestEasyServices(){

        //this.singletons.add(new HelloWorldResource());
        //this.classes = getClasses();

    }

    //@Override
    //public Set<Object> getSingletons() {
    //    return singletons;
    //}

    @Override
    public Set<Class<?>> getClasses() { //aixo no son instancies crec! a singletons si... cambiaria el comportament?
        HashSet<Class<?>> ret = new HashSet<Class<?>>();

        ret.addAll(Arrays.asList(HelloWorldResource.class));

        return ret;
    }
}
