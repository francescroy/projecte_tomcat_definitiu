package prova_gradle_war;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Servlet1
 */
public class Servlet1 extends HttpServlet {


       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Servlet1() {
        super();
        // TODO Auto-generated constructor stub
    }



	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.getServletContext().log("Entrando en Servlet 1");

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		HttpSession session = request.getSession(false);
		if (session != null) {
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Hola</title>");
			out.println("</head>");
			out.println("<body bgcolor=\"white\">");
			out.println("<h2>Hi from Servlet 1. You are already logged.</h2>");
			out.println("</body>");
			out.println("</html>");

		} else {

			String param1 = request.getParameter("param1");
			String param2 = request.getParameter("param2");
			if(param1.equals("Juan") && param2.equals("password1234")) {

				session = request.getSession();
				session.setAttribute("username", param1);

				out.println("<html>");
				out.println("<head>");
				out.println("<title>Hola</title>");
				out.println("</head>");
				out.println("<body bgcolor=\"white\">");
				out.println("<h2>Hi from Servlet 1.Logged successfully</h2>");
				out.println("</body>");
				out.println("</html>");
			}else{


				out.println("<html>");
				out.println("<head>");
				out.println("<title>Hola</title>");
				out.println("</head>");
				out.println("<body bgcolor=\"white\">");
				out.println("<h2>Hi from Servlet 1. Logged NOT successfully.</h2>");
				out.println("</body>");
				out.println("</html>");


			}
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
