package prova_gradle_war;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet implementation class Servlet2
 */
public class Servlet2 extends HttpServlet {



    /**
     * @see HttpServlet#HttpServlet()
     */
    public Servlet2() {
        super();
        // TODO Auto-generated constructor stub
    }



	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		HttpSession session = request.getSession(false);
		if (session != null) {
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Hola</title>");
			out.println("</head>");
			out.println("<body bgcolor=\"white\">");
			out.println("<h2>Hi "+session.getAttribute("username")+" from Servlet 2 "+(new Library()).someLibraryMethod()+"</h2>");
			out.println("</body>");
			out.println("</html>");

		} else {
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Hola</title>");
			out.println("</head>");
			out.println("<body bgcolor=\"white\">");
			out.println("<h2>Permission denied!</h2>");
			out.println("</body>");
			out.println("</html>");
		}



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
