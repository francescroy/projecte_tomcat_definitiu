package package1;

import javax.xml.bind.annotation.*;
import java.util.Date;

@XmlRootElement(name = "book")
public class Book {
    private Long id;
    private String name;
    private String author;

    @XmlAttribute
    public void setId(Long id) {
        this.id = id;
    }

    @XmlElement(name = "title")
    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "author")
    public void setAuthor(String author) {
        this.author = author;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    // constructor, getters and setters
}